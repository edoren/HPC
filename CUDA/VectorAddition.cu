#include <cuda.h>
#include <chrono>
#include <cstring>
#include <iostream>

#define NUM_THREADS 1024

// nvcc -std=c++11 -O3 -D_FORCE_INLINES  VectorAddition.cu -o VectorAddition && ./VectorAddition

__global__ void SumDevice(int* a, int* b, int* c, size_t size) {
    size_t id = blockIdx.x * blockDim.x + threadIdx.x;
    if (id < size) {
        c[id] = a[id] + b[id];
    }
}

void SumHost(int* a, int* b, int* c, size_t size) {
    for (int i = 0; i < size; i++) {
        c[i] = a[i] + b[i];
    }
}

int main() {
    size_t size = 1024 * 1024;

    int *h_a, *h_b, *h_c;
    int *d_a, *d_b, *d_c;

    size_t bytes = size * sizeof(int);

    h_a = new int[size];
    h_b = new int[size];
    h_c = new int[size];

    for (int i = 0; i < size; i++) {
        h_a[i] = h_b[i] = i;
    }

    cudaMalloc((void**)&d_a, bytes);
    cudaMalloc((void**)&d_b, bytes);
    cudaMalloc((void**)&d_c, bytes);

    cudaMemcpy(d_a, h_a, bytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, h_b, bytes, cudaMemcpyHostToDevice);

    {
        auto start = std::chrono::high_resolution_clock::now();
        SumHost(h_a, h_b, h_c, size);
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed =
            std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
        std::cout << "Host: " << elapsed.count() << "ns\n";
    }

    int num_blocks =
        static_cast<int>(std::ceil(static_cast<float>(size) / NUM_THREADS));

    {
        auto start = std::chrono::high_resolution_clock::now();
        SumDevice<<<num_blocks, NUM_THREADS>>>(d_a, d_b, d_c, size);
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed =
            std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
        std::cout << "Device: " << elapsed.count() << "ns\n";
    }

    cudaMemcpy(h_c, d_c, bytes, cudaMemcpyDeviceToHost);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    delete h_a;
    delete h_b;
    delete h_c;

    return 0;
}
