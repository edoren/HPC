#include <chrono>
#include <iostream>

#include <cuda.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

// nvcc -std=c++11 -O3 -D_FORCE_INLINES  GrayScale.cu -o GrayScale && ./GrayScale

__device__ size_t getGlobalIdx_2D_2D() {
    size_t blockId = blockIdx.x + blockIdx.y * gridDim.x;
    size_t threadId = blockId * (blockDim.x * blockDim.y) +
                      (threadIdx.y * blockDim.x) + threadIdx.x;
    return threadId;
}

__global__ void ToGrayscaleDevice(const uint8_t* data, uint8_t* output,
                                  size_t num_pixels) {
    size_t id = getGlobalIdx_2D_2D();
    if (id < num_pixels) {
        int rgboffset = id * 3;
        uint8_t r = data[rgboffset + 2];
        uint8_t g = data[rgboffset + 1];
        uint8_t b = data[rgboffset + 0];
        float luminance = 0.2989 * r + 0.5870 * g + 0.1140 * b;
        output[id] = static_cast<uint8_t>(luminance);
    }
}

void ToGrayscaleHost(const uint8_t* data, uint8_t* output, size_t num_pixels) {
    for (int i = 0; i < num_pixels; i++) {
        int rgboffset = i * 3;
        uint8_t r = data[rgboffset + 2];
        uint8_t g = data[rgboffset + 1];
        uint8_t b = data[rgboffset + 0];
        float luminance = 0.2989 * r + 0.5870 * g + 0.1140 * b;
        output[i] = static_cast<uint8_t>(luminance);
    }
}

#define MEASURE_TIME(FUNCTION)                                                 \
    {                                                                          \
        auto start = std::chrono::high_resolution_clock::now();                \
        (FUNCTION);                                                            \
        auto end = std::chrono::high_resolution_clock::now();                  \
        auto elapsed =                                                         \
            std::chrono::duration_cast<std::chrono::nanoseconds>(end - start); \
        std::cout << "the line " << __LINE__ << " took " << elapsed.count()    \
                  << "ns.\n";                                                  \
    }

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " image_file" << std::endl;
        return -1;
    }

    // Read the file and check if is valid
    cv::Mat image_rgb = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);
    if (!image_rgb.data) {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    // Create the image to store the output
    cv::Mat image_gray(image_rgb.rows, image_rgb.cols, CV_8UC1);
    int num_pixels = image_rgb.cols * image_rgb.rows;

    // Reserve space for the Device
    uint8_t* d_image_rgb_data;
    uint8_t* d_image_gray_data;
    cudaMalloc((void**)&d_image_rgb_data, num_pixels * 3);
    cudaMalloc((void**)&d_image_gray_data, num_pixels);

    // Copy the image data to the device
    cudaMemcpy(d_image_rgb_data, image_rgb.data, num_pixels * 3,
               cudaMemcpyHostToDevice);

    // Execute the Host version of the code
    ToGrayscaleHost(image_rgb.data, image_gray.data, num_pixels);

    // Save the Host output to a file
    cv::imwrite("out_host.png", image_gray);

    dim3 dimBlock(image_rgb.rows, image_rgb.cols, 1);
    dim3 dimGrid(image_rgb.rows, image_rgb.cols, 1);

    // Execute the Device version of the code
    int num_threads = 1024;
    int num_blocks = (int)std::ceil(num_pixels / (float)num_threads);
    ToGrayscaleDevice<<<dimGrid, num_threads>>>(
        d_image_rgb_data, d_image_gray_data, num_pixels * 3);

    // Save Device output to a file
    cudaMemcpy(image_gray.data, d_image_gray_data, num_pixels,
               cudaMemcpyDeviceToHost);
    cv::imwrite("out_device.png", image_gray);

    // Free the Device memory
    cudaFree(d_image_rgb_data);
    cudaFree(d_image_gray_data);

    return 0;
}
