#pragma once

#include <cstring>
#include <sstream>
#include <ostream>
#include <stdexcept>

#include <iostream>

template <typename T>
class Matrix {
public:
    Matrix() : num_rows_(0), num_cols_(0), data_(nullptr) {}

    Matrix(size_t num_rows, size_t num_cols)
          : num_rows_(num_rows), num_cols_(num_cols) {
        data_ = new T[num_rows_ * num_cols_];
    }

    Matrix(const Matrix<T>& other)
          : num_rows_(other.num_rows_), num_cols_(other.num_cols_) {
        data_ = new T[num_rows_ * num_cols_];
        memcpy(data_, other.data_, num_rows_ * num_cols_ * sizeof(T));
    };

    Matrix(Matrix<T>&& other)
          : num_rows_(other.num_rows_),
            num_cols_(other.num_cols_),
            data_(other.data_) {
        other.num_rows_ = 0;
        other.num_cols_ = 0;
        other.data_ = nullptr;
    };

    ~Matrix() {
        delete[] data_;
        data_ = nullptr;
    }

    Matrix<T>& operator=(const Matrix<T>& other) {
        if (this != &other) {
            num_rows_ = other.num_rows_;
            num_cols_ = other.num_cols_;
            delete[] data_;
            data_ = new T[num_rows_ * num_cols_];
            memcpy(data_, other.data_, num_rows_ * num_cols_ * sizeof(T));
        }
        return *this;
    }

    Matrix<T>& operator=(Matrix<T>&& other) {
        if (this != &other) {
            num_rows_ = other.num_rows_;
            num_cols_ = other.num_cols_;
            delete[] data_;
            data_ = other.data_;
            other.num_rows_ = 0;
            other.num_cols_ = 0;
            other.data_ = nullptr;
        }
        return *this;
    };

    size_t NumRows() const {
        return num_rows_;
    }

    size_t NumCols() const {
        return num_cols_;
    }

    T* Data() {
        return data_;
    }

    const T* Data() const {
        return data_;
    }

    T& operator()(size_t row, size_t col) {
        if (row >= num_rows_ || col >= num_cols_) {
            std::stringstream stream;
            stream << "Out of matrix bounds. Position [" << row << ", " << col
                   << "] does not exist.";
            throw std::runtime_error(stream.str());
        }
        return data_[row * num_cols_ + col];
    }

    const T& operator()(size_t row, size_t col) const {
        return const_cast<const T&>(const_cast<Matrix<T>&>(*this)(row, col));
    }

private:
    size_t num_rows_;
    size_t num_cols_;
    T* data_;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& m) {
    os << '[';
    for (size_t j = 0; j < m.NumRows(); j++) {
        if (j != 0) os << ' ';
        os << '[';
        for (size_t i = 0; i < m.NumCols(); i++) {
            os << m(j, i);
            if (i != m.NumCols() - 1) os << ", ";
        }
        os << ']';
        if (j != m.NumRows() - 1) os << '\n';
    }
    os << ']';
    return os;
}
