cmake_minimum_required(VERSION 3.0)

# Create the CMake project
project(MultMatrix)

# Set the output folders
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

# Set the build type
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()
message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_definitions("-DDEBUG")
endif()

# Find required libraries
find_package(MPI REQUIRED)
find_package(CUDA REQUIRED)

# Handle Warnings and C++11 Support
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra")
list(APPEND CUDA_NVCC_FLAGS "-std=c++11;-Wno-deprecated-gpu-targets")
set(CUDA_PROPAGATE_HOST_FLAGS OFF)

# Directories to include
include_directories(SYSTEM ${MPI_C_INCLUDE_PATH}
                           ${MPI_CXX_INCLUDE_PATH}
                           ${CUDA_INCLUDE_DIRS})

# Compile CUDA files
cuda_compile(CudaMatrixMult CudaMatrixMult.cu)

# Create the executable and link the libraries
set(EXECUTABLE_NAME "MultMatrix")
add_executable(${EXECUTABLE_NAME} MultMatrix.cpp ${CudaMatrixMult})
target_link_libraries(${EXECUTABLE_NAME} ${MPI_C_LIBRARIES}
                                         ${MPI_CXX_LIBRARIES}
                                         ${CUDA_LIBRARIES})

# Create the Slurm batch files
set(NUM_NODES "4")
set(ARGS "--gpu")
set(NTASKS_PER_NODE "1")
configure_file(MultMatrix.sh.in MultMatrixGPU.sh @ONLY)
set(ARGS "--cpu")
set(NTASKS_PER_NODE "8")
configure_file(MultMatrix.sh.in MultMatrixCPU.sh @ONLY)
