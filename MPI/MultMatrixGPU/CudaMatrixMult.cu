#include <cuda.h>

#include <cmath>
#include <cstdio>

#include "Matrix.hpp"

#if defined(_DEBUG) || DEBUG == 1
#define LOG_CUDA_ERRORS
#endif

#ifdef LOG_CUDA_ERRORS
#define CUDA_CALL(call) \
    { LogCUDAError(__FILE__, __LINE__, #call, call); }
#else
#define CUDA_CALL(call) call
#endif

void LogCUDAError(const char* file, int line, const char* call,
                  cudaError_t status) {
    if (status != cudaSuccess) {
        printf("%s(%d): CUDA Error: %i from %s\n", file, line, status, call);
    }
}

__global__ void CudaMultMatrixKernel(const double* d_vec_a,
                                     const double* d_mat_b, double* d_vec_c,
                                     size_t num_cols_a, size_t num_cols_b) {
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;

    if (col < num_cols_b) {
        double result = 0;
        for (size_t k = 0; k < num_cols_a; k++) {
            result += d_vec_a[k] * d_mat_b[k * num_cols_b + col];
        }
        d_vec_c[col] = result;
    }
}

Matrix<double> CudaMultMatrix(const Matrix<double>& mat_a,
                              const Matrix<double>& mat_b) {
    double *d_vec_a, *d_mat_b, *d_vec_c;

    size_t vec_a_size = mat_a.NumCols() * sizeof(double);
    size_t mat_b_size = mat_b.NumRows() * mat_b.NumCols() * sizeof(double);
    size_t vec_c_size = mat_b.NumCols() * sizeof(double);

    CUDA_CALL(cudaMalloc(&d_vec_a, vec_a_size));
    CUDA_CALL(cudaMalloc(&d_mat_b, mat_b_size));
    CUDA_CALL(cudaMalloc(&d_vec_c, vec_c_size));

    CUDA_CALL(
        cudaMemcpy(d_mat_b, mat_b.Data(), mat_b_size, cudaMemcpyHostToDevice));

    Matrix<double> result(mat_a.NumRows(), mat_b.NumCols());

    const int num_threads = 1024;
    const int num_blocks = static_cast<int>(
        std::ceil(mat_b.NumCols() / static_cast<float>(num_threads)));
    for (size_t i = 0; i < mat_a.NumRows(); i++) {
        const double* source = &mat_a.Data()[i * mat_a.NumCols()];
        double* destination = &result.Data()[i * mat_b.NumCols()];

        CUDA_CALL(
            cudaMemcpy(d_vec_a, source, vec_a_size, cudaMemcpyHostToDevice));

        CudaMultMatrixKernel<<<num_blocks, num_threads>>>(
            d_vec_a, d_mat_b, d_vec_c, mat_a.NumCols(), mat_b.NumCols());

        CUDA_CALL(cudaMemcpy(destination, d_vec_c, vec_c_size,
                             cudaMemcpyDeviceToHost));
    }

    CUDA_CALL(cudaFree(d_vec_a));
    CUDA_CALL(cudaFree(d_mat_b));
    CUDA_CALL(cudaFree(d_vec_c));

    return result;
}
