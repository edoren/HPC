#include <mpi.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <sstream>
#include <string>

#include <ctime>

#include "Matrix.hpp"

#define MASTER 0
#define FROM_MASTER 1
#define FROM_WORKER 2

typedef std::chrono::duration<double> duration_seconds;

Matrix<double> CudaMultMatrix(const Matrix<double>& mat_a,
                              const Matrix<double>& mat_b);

std::string GetNodeName() {
    char name[MPI_MAX_PROCESSOR_NAME];
    int namelen;
    MPI_Get_processor_name(name, &namelen);
    return std::string(name, namelen);
}

std::string GetCurrentDate(const char* fmt) {
    std::time_t t = std::time(nullptr);
    std::tm* tm = std::localtime(&t);
    char mbstr[100];
    std::strftime(mbstr, 100, fmt, tm);
    return std::string(mbstr);
}

int main(int argc, char* argv[]) {
    int taskid;
    int numtasks;
    MPI_Status status;

    bool use_gpu = false;
    bool show_results = false;

    for (int i = 1; i < argc; i++) {
        std::string arg(argv[i]);
        if (arg == "--cpu")
            use_gpu = false;
        else if (arg == "--gpu")
            use_gpu = true;
        else if (arg == "--results")
            show_results = true;
    }

    size_t NRA = 16000;
    size_t NCA = 16000;
    size_t NCB = 16000;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    if (numtasks < 2) {
        int rc = 0;
        std::cout << "Need at least two MPI tasks. Quitting..." << '\n';
        MPI_Abort(MPI_COMM_WORLD, rc);
        exit(1);
    }

    if (taskid == MASTER) {
        Matrix<double> a(NRA, NCA);
        Matrix<double> b(NCA, NCB);
        Matrix<double> c(a.NumRows(), b.NumCols());

        if (use_gpu) {
            std::cout << "Using GPU." << '\n';
        } else {
            std::cout << "Using CPU." << '\n';
        }

        std::string date = GetCurrentDate("%c %Z");
        std::cout << "Running Date: " << date << '\n' << '\n';

        std::cout << "Initializing matrices of size [" << a.NumRows() << ", "
                  << a.NumCols() << "] and [" << b.NumRows() << ", "
                  << b.NumCols() << "]... " << std::flush;
        size_t max_size =
            std::max(a.NumCols() * a.NumRows(), b.NumCols() * b.NumRows());
        for (size_t i = 0; i < max_size; i++) {
            if (i < a.NumCols() * a.NumRows()) a.Data()[i] = 1.f;
            if (i < b.NumCols() * b.NumRows()) b.Data()[i] = 1.f;
        }
        std::cout << "Done." << '\n';

        if (show_results == true) {
            std::cout << "Mat A" << '\n';
            std::cout << a << '\n';
            std::cout << "Mat B" << '\n';
            std::cout << b << '\n' << '\n';
        }

        int numworkers = numtasks - 1;
        int averow = a.NumRows() / numworkers;
        int extra = a.NumRows() % numworkers;
        int offset = 0;
        int num_rows = 0;

        // Save the start time
        auto start = std::chrono::high_resolution_clock::now();

        // Send matrix data to the worker tasks
        for (int dest = 1; dest <= numworkers; dest++) {
            if (dest <= extra) {
                num_rows = averow + 1;
            } else {
                num_rows = averow;
            }
            std::cout << ">>> Sending to task " << dest << " offset=" << offset
                      << " rows=" << num_rows << '\n';
            MPI_Send(&offset, 1, MPI_INT, dest, FROM_MASTER, MPI_COMM_WORLD);
            MPI_Send(&num_rows, 1, MPI_INT, dest, FROM_MASTER, MPI_COMM_WORLD);
            MPI_Send(&(a.Data()[offset * a.NumCols()]), num_rows * a.NumCols(),
                     MPI_DOUBLE, dest, FROM_MASTER, MPI_COMM_WORLD);
            MPI_Send(b.Data(), b.NumRows() * b.NumCols(), MPI_DOUBLE, dest,
                     FROM_MASTER, MPI_COMM_WORLD);
            offset = offset + num_rows;
        }

        // Receive results from worker tasks
        for (int source = 1; source <= numworkers; source++) {
            MPI_Recv(&offset, 1, MPI_INT, source, FROM_WORKER, MPI_COMM_WORLD,
                     &status);
            MPI_Recv(&num_rows, 1, MPI_INT, source, FROM_WORKER, MPI_COMM_WORLD,
                     &status);
            MPI_Recv(&c.Data()[offset * b.NumCols()], num_rows * b.NumCols(),
                     MPI_DOUBLE, source, FROM_WORKER, MPI_COMM_WORLD, &status);
            std::cout << "<<< Received results from task " << source << '\n';
        }

        std::cout << '\n';

        // Save the end time
        auto end = std::chrono::high_resolution_clock::now();
        auto elap = std::chrono::duration_cast<duration_seconds>(end - start);
        std::cout << "Elapsed time: " << elap.count() << "s\n";

        // Print results
        if (show_results == true) {
            std::cout << "*******************************************" << '\n';
            std::cout << "Result Matrix:" << '\n';
            std::cout << c << '\n';
            std::cout << "*******************************************" << '\n';
        }

        // Verify if the solution is correct
        bool correct = true;
        double result = static_cast<double>(NCA);
        size_t num_values_c = c.NumRows() * c.NumCols();
        for (size_t i = 0; i < num_values_c; i++) {
            if (c.Data()[i] != result) {
                std::cout << c.Data()[i] << '\n';
                correct = false;
                break;
            }
        }
        std::cout << "The Matrix values are "
                  << (correct ? "Correct" : "Incorrect") << '\n';
    }

    // Worker Task
    else {
        int offset;
        int num_rows;
        MPI_Recv(&offset, 1, MPI_INT, MASTER, FROM_MASTER, MPI_COMM_WORLD,
                 &status);
        MPI_Recv(&num_rows, 1, MPI_INT, MASTER, FROM_MASTER, MPI_COMM_WORLD,
                 &status);

        Matrix<double> a(num_rows, NCA);
        Matrix<double> b(NCA, NCB);

        MPI_Recv(a.Data(), a.NumRows() * a.NumCols(), MPI_DOUBLE, MASTER,
                 FROM_MASTER, MPI_COMM_WORLD, &status);
        MPI_Recv(b.Data(), b.NumRows() * b.NumCols(), MPI_DOUBLE, MASTER,
                 FROM_MASTER, MPI_COMM_WORLD, &status);

        Matrix<double> c;
        if (use_gpu) {
            c = CudaMultMatrix(a, b);
        } else {
            c = Matrix<double>(a.NumRows(), b.NumCols());
            for (size_t i = 0; i < a.NumRows(); i++) {
                for (size_t j = 0; j < b.NumCols(); j++) {
                    double result = 0;
                    for (size_t k = 0; k < a.NumCols(); k++) {
                        result += a(i, k) * b(k, j);
                    }
                    c(i, j) = result;
                }
            }
        }

        MPI_Send(&offset, 1, MPI_INT, MASTER, FROM_WORKER, MPI_COMM_WORLD);
        MPI_Send(&num_rows, 1, MPI_INT, MASTER, FROM_WORKER, MPI_COMM_WORLD);
        MPI_Send(c.Data(), c.NumRows() * c.NumCols(), MPI_DOUBLE, MASTER,
                 FROM_WORKER, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
