#include <cuda.h>
#include <ostream>

void DisplayCUDAHeader(std::ostream& stream) {
    const int kb = 1024;
    const int mb = kb * kb;

    int version = 0;
    cudaDriverGetVersion(&version);
    stream << "CUDA Driver: v" << (version / 1000) << '.'
           << ((version % 100) / 10) << '\n';
    cudaRuntimeGetVersion(&version);
    stream << "CUDA Runtime: v" << (version / 1000) << '.'
           << ((version % 100) / 10) << '\n';

    stream << '\n';

    int devCount;
    cudaGetDeviceCount(&devCount);
    stream << "CUDA Devices: " << '\n' << '\n';

    if (devCount == 0) stream << "NONE" << '\n' << '\n';

    for (int i = 0; i < devCount; ++i) {
        cudaDeviceProp props;
        cudaGetDeviceProperties(&props, i);
        stream << "Device No. " << i << '\n';
        stream << "  " << props.name << ": " << props.major << '.'
               << props.minor << '\n';
        stream << "  Global memory:   " << props.totalGlobalMem / mb << "mb"
               << '\n';
        stream << "  Shared memory:   " << props.sharedMemPerBlock / kb << "kb"
               << '\n';
        stream << "  Constant memory: " << props.totalConstMem / kb << "kb"
               << '\n';
        stream << "  Block registers: " << props.regsPerBlock << '\n' << '\n';

        stream << "  Warp size:         " << props.warpSize << '\n';
        stream << "  Threads per block: " << props.maxThreadsPerBlock << '\n';
        stream << "  Max block dimensions: [ " << props.maxThreadsDim[0] << ", "
               << props.maxThreadsDim[1] << ", " << props.maxThreadsDim[2]
               << " ]" << '\n';
        stream << "  Max grid dimensions:  [ " << props.maxGridSize[0] << ", "
               << props.maxGridSize[1] << ", " << props.maxGridSize[2] << " ]"
               << '\n';
        if (i < devCount - 1) stream << '\n';
    }
}
