#include <ctime>

#include <iostream>
#include <sstream>
#include <string>

#include <mpi.h>

#define MASTER 0
#define FROM_MASTER 1
#define FROM_WORKER 2

void DisplayCUDAHeader(std::ostream& stream);

std::string GetNodeName() {
    char name[MPI_MAX_PROCESSOR_NAME];
    int namelen;
    MPI_Get_processor_name(name, &namelen);
    return std::string(name, namelen);
}

std::string GetCurrentDate(const char* fmt) {
    std::time_t t = std::time(nullptr);
    std::tm* tm = std::localtime(&t);
    char mbstr[100];
    std::strftime(mbstr, 100, fmt, tm);
    return std::string(mbstr);
}

int main(int argc, char* argv[]) {
    int numtasks, taskid, numworkers;

    MPI_Status status;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    std::string node_name = GetNodeName();

    if (numtasks < 2) {
        int rc = 0;
        std::cout << "Need at least two MPI tasks. Quitting..." << '\n';
        MPI_Abort(MPI_COMM_WORLD, rc);
        exit(1);
    }

    numworkers = numtasks - 1;

    if (taskid == MASTER) {
        std::cout << "##################################################\n";
        std::cout << "Date: " << GetCurrentDate("%c %Z") << '\n' << '\n';

        std::cout << "Master Node: " << node_name << '\n' << '\n';

        std::cout << "Number tasks: " << numtasks << '\n';
        std::cout << "Number workers: " << numworkers << '\n' << '\n';

        DisplayCUDAHeader(std::cout);
        std::cout << "##################################################\n";

        std::cout << '\n';

        for (int source = 1; source <= numworkers; source++) {
            std::string str;
            unsigned size = 0;
            MPI_Recv(&size, 1, MPI_UNSIGNED, source, FROM_WORKER,
                     MPI_COMM_WORLD, &status);
            str.resize(size);
            MPI_Recv(&str[0], size, MPI_CHAR, source, FROM_WORKER,
                     MPI_COMM_WORLD, &status);
            std::cout << "<< Received results from task " << source << '\n';
            std::cout << str << std::endl;
        }
    } else if (taskid > MASTER) {
        std::stringstream stream;
        stream << "==================================================\n";
        stream << "Task: " << taskid << '\n';
        stream << "Current Node: " << node_name << '\n' << '\n';
        DisplayCUDAHeader(stream);
        stream << "==================================================\n";

        std::string str = stream.str();
        unsigned size = str.size();

        MPI_Send(&size, 1, MPI_UNSIGNED, MASTER, FROM_WORKER, MPI_COMM_WORLD);
        MPI_Send(&str[0], size, MPI_CHAR, MASTER, FROM_WORKER, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
