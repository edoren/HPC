#include <mpi.h>

#include <cstdint>

#include <vector>
#include <random>
#include <iostream>
#include <algorithm>

#define SIZE 100000000

#define MASTER 0 /* taskid of first task */

#define FROM_MASTER 1 /* setting a message type */
#define FROM_WORKER 2 /* setting a message type */

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v) {
    os << "[";
    for (uint64_t i = 0; i < v.size(); i++) {
        os << v[i];
        if (i != v.size() - 1) os << ", ";
    }
    os << "]";
    return os;
}

template <typename T>
std::vector<T> RandomVector(uint64_t size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> val(0, size);

    std::vector<T> vec(size);
    std::generate(vec.begin(), vec.end(),
                  [&] { return static_cast<T>(val(gen)); });

    return vec;
}

int main(int argc, char* argv[]) {
    int numtasks;         // number of tasks in partition
    int taskid;           // a task identifier
    uint64_t numworkers;  // number of worker tasks
    int mtype;            // message type

    std::vector<double> a;
    std::vector<double> b;
    std::vector<double> c;

    MPI_Status status;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    if (numtasks < 2) {
        int rc = 0;
        std::cout << "Need at least two MPI tasks. Quitting..." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, rc);
        exit(1);
    }
    numworkers = numtasks - 1;

    /** master task **/
    if (taskid == MASTER) {
        a = RandomVector<double>(SIZE);  // matrix A to be multiplied
        b = RandomVector<double>(SIZE);  // matrix B to be multiplied
        c.resize(SIZE, 0.0);             // result matrix C

        /* Send matrix data to the worker tasks */
        uint64_t averow = static_cast<uint64_t>(SIZE / numworkers);
        uint64_t extra = SIZE % numworkers;
        uint64_t offset = 0;
        uint64_t num_values = 0;

        mtype = FROM_MASTER;
        for (uint64_t dest = 1; dest <= numworkers; dest++) {
            num_values = (dest <= extra) ? averow + 1 : averow;
            std::cout << "Sending " << num_values << " num_values to task "
                      << dest << " offset=" << offset << std::endl;
            MPI_Send(&offset, 1, MPI_UINT64_T, dest, mtype, MPI_COMM_WORLD);
            MPI_Send(&num_values, 1, MPI_UINT64_T, dest, mtype, MPI_COMM_WORLD);
            MPI_Send(&a[offset], num_values, MPI_DOUBLE, dest, mtype,
                     MPI_COMM_WORLD);
            MPI_Send(&b[offset], num_values, MPI_DOUBLE, dest, mtype,
                     MPI_COMM_WORLD);
            offset = offset + num_values;
        }

        /* Receive results from worker tasks */
        mtype = FROM_WORKER;
        for (uint64_t source = 1; source <= numworkers; source++) {
            MPI_Recv(&offset, 1, MPI_UINT64_T, source, mtype, MPI_COMM_WORLD,
                     &status);
            MPI_Recv(&num_values, 1, MPI_UINT64_T, source, mtype,
                     MPI_COMM_WORLD, &status);
            MPI_Recv(&c[offset], num_values, MPI_DOUBLE, source, mtype,
                     MPI_COMM_WORLD, &status);
            std::cout << "Received results from task " << source << std::endl;
        }

        /* Print results */
        // std::cout << c << std::endl;
    }

    /** worker task **/
    if (taskid > MASTER) {
        mtype = FROM_MASTER;
        uint64_t num_values, offset;
        MPI_Recv(&offset, 1, MPI_UINT64_T, MASTER, mtype, MPI_COMM_WORLD,
                 &status);
        MPI_Recv(&num_values, 1, MPI_UINT64_T, MASTER, mtype, MPI_COMM_WORLD,
                 &status);
        a.resize(num_values, 0.0);
        b.resize(num_values, 0.0);
        c.resize(num_values, 0.0);
        MPI_Recv(&a[0], num_values, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD,
                 &status);
        MPI_Recv(&b[0], num_values, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD,
                 &status);

        for (uint64_t i = 0; i < num_values; i++) {
            c[i] = a[i] + b[i];
        }
        mtype = FROM_WORKER;

        MPI_Send(&offset, 1, MPI_UINT64_T, MASTER, mtype, MPI_COMM_WORLD);
        MPI_Send(&num_values, 1, MPI_UINT64_T, MASTER, mtype, MPI_COMM_WORLD);
        MPI_Send(&c[0], num_values, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
