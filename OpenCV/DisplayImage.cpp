#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

// g++ DisplayImage.cpp -o DisplayImage $(pkg-config --libs opencv)

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " image_file" << std::endl;
        return -1;
    }

    cv::Mat image;
    image = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);  // Read the file

    // Check for invalid input
    if (!image.data) {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    // for (int i = 0; i < image.cols * image.rows * 3; i+=3){
    //     image.data[i] = 0;
    // }

    cv::namedWindow("Display window",
                    cv::WINDOW_AUTOSIZE);  // Create a window for display.
    cv::imshow("Display window", image);   // Show our image inside it.

    cv::waitKey(0);  // Wait for a keystroke in the window

    return 0;
}
